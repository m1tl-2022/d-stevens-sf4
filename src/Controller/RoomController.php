<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Form\RoomType;
use App\Form\SearchType;
use App\Repository\RoomRepository;
use App\Entity\Room;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class RoomController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    /**
     * @Route("/room", name="room")
     */
    public function index(RoomRepository $roomRepository, Request $request): Response
    {

        $data = new SearchData();
        $data->page = $request->get('page', 1);
        $form = $this->createForm(SearchType::class, $data);
        $form->handleRequest($request);
        $rooms = $roomRepository->findQueryResult($data);

        if($form->get('clear')->isClicked()){
            return $this->redirectToRoute('room');
        }

        return $this->render('room/index.html.twig', [
            'controller_name' => 'RoomController',
            'rooms' =>$rooms,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/new", name="room.add", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     *
     */
    public function new(Request $request) : Response
    {
        $room = new Room();
        $room->setUser($this->security->getUser());
        $room->setCreatedAt(date_create('now'));
        $room->setUpdatedAt(date_create('now'));

        $form = $this->createForm(RoomType::class, $room);
        $room->setUpdatedAt(date_create('now'));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();

            $this->addFlash('success', 'La salle a bien été ajoutée.');
            return $this->redirectToRoute('room');
        }

        return $this->render('room/new.html.twig', [
            'room'=>$room,
            'roomForm'=>$form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="room.edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     *
     */
    public function edit(Request $request, Room $room): Response
    {
        $room->setUpdatedAt(date_create('now'));
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Les modifications ont bien été pris en compte.');
           return $this->redirectToRoute('room');
        }


        return $this->render('room/edit.html.twig', [
            'room'=>$room,
            'roomForm'=>$form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="room.delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     *
     */
    public function delete(Request $request, Room $room):Response
    {
        if ($this->isCsrfTokenValid('delete'.$room->getId(), $request->request->get('_token'))){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();

            $this->addFlash('success', 'La salle a bien été supprimée.');
        }
        return $this->redirectToRoute('room');
    }

    /**
     * @Route("/room/{city}", name="room.showCity")
     * @return Response
     */
    public function showRoomsByCity(RoomRepository $repository,$city): Response
    {
        $rooms = $repository->findByCity($city);

        return $this->render('room/showCity.html.twig', [
            'rooms'=>$rooms
        ]);
    }
}



